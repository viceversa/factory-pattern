const root = document.getElementById('root');

// data factory
const makeTableFactory = document => rows => {
  const table = document.createElement('table');
  appendRows(table, rows);
  return table;
}

const appendRows = (table, rows) => {
  rows.forEach((rowData, index) => {
    const row = table.insertRow(index);
    appendCells(row, rowData);
  });
};

const appendCells = (row, rowData) => {
  rowData.forEach((text, index) => {
    const cell = row.insertCell(index);
    cell.innerHTML = text;
  });
};

const table = makeTableFactory(document);

const data = [
  ['foo', 'bar', ' baz'],
  ['foo', 'bar', ' baz'],
  ['foo', 'bar', ' baz'],
]

root.appendChild(table(data));

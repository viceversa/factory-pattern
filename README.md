# Factory Pattern

## Data-driven Factory

Returns an instance of some type of data or object and it will be differend depending on the input data.

## Label Factory

Returns an instance of some type of data or object depending on the input string of text or "label". For example
createElement("DIV") is a factory function that creates an element based on provided label (element name)

Thanks to Fredrik Christenson: [YouTube video](https://youtu.be/0QrejxjKWy4)

// const root = document.getElementById('root');

const checkType = label => thing => {
  return typeof thing === label;
};

const isNode = checkType('node');
const isString = checkType('string');
const isObject = checkType('object');

const maybeChild = child => {
  return isNode(child) || (isObject(child) && !Array.isArray(child));
}

const applyAttributes = (node, attributes) => {
  if (!isObject(attributes)) return;
  const keys = Object.keys(attributes);
  keys.forEach(key => {
    node.setAttribute(key, attributes[key]);
  });
};

// label factory
const makeElement = (document, elementName) => (children, attributes) => {
  const element = document.createElement(elementName);
  applyAttributes(element, attributes);
  if (maybeChild(children)) {
    element.appendChild(children);
  } else if (Array.isArray(children)) {
    children.forEach(child => {
      element.appendChild(child);
    });
  } else if (isString(children)) {
    element.innerHTML = children;
  }
  return element;
}

const h1 = makeElement(document, 'h1');
const h2 = makeElement(document, 'h2');
const h3 = makeElement(document, 'h3');
const ul = makeElement(document, 'ul');
const li = makeElement(document, 'li');
const div = makeElement(document, 'div');

root.appendChild(h1('foo', {style: 'background: hotpink; color: white;'}));
root.appendChild(h2('bar'));
root.appendChild(h3(h2('baz')));
const list = [li('foo'), li('bar'), li('baz')];
root.appendChild(ul(list));

// create component
const MyComponent = () => {
  const data = [li('foo'), li('bar'), li('baz')];
  const list = ul(data);
  const header = h1('Some stuff', {style: 'background: hotpink; color: white;' });
  const children = [header, list];
  return div(children);
}

root.appendChild(MyComponent());